import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:movies/bloc/movies_bloc.dart';
import 'package:movies/model/movies.dart';
import 'package:movies/utils/constants.dart';
import 'package:movies/utils/extensions.dart';

class HomeDetail extends StatefulWidget {
  HomeDetail(
      {required this.name,
      required this.director,
      required this.summary,
      required this.genres,
      Key? key})
      : super(key: key);

  String name, director, summary, genres;

  @override
  State<HomeDetail> createState() => _HomeDetailState();
}

class _HomeDetailState extends State<HomeDetail> {
  late TextEditingController _namecontroller;
  late TextEditingController _directorcontroller;
  late TextEditingController _summarycontroller;

  List<String> genreSelected = [];

  @override
  void initState() {
    super.initState();
    _namecontroller = TextEditingController(text: widget.name.trim());
    _directorcontroller = TextEditingController(text: widget.director.trim());
    _summarycontroller = TextEditingController(text: widget.summary.trim());

    if (widget.genres.trim().isNotEmpty) {
      setState(() {
        genreSelected.addAll(widget.genres.split(", "));
      });
    }
  }

  @override
  void dispose() {
    _namecontroller.dispose();
    _directorcontroller.dispose();
    _summarycontroller.dispose();
    super.dispose();
  }

  void addMovie() {
    if (_namecontroller.text.trim().isEmpty) {
      context.showMessageDialog(nameCannotBeEmpty);
      return;
    }

    if (_directorcontroller.text.isEmpty) {
      context.showMessageDialog(directorCannotBeEmpty);
      return;
    }

    if (_summarycontroller.text.isEmpty) {
      context.showMessageDialog(summaryCannotBeEmpty);
      return;
    }

    if (genreSelected.isEmpty) {
      context.showMessageDialog(genreCannotBeEmpty);
      return;
    }
    moviesBloc.moviesListValue?.add(Movies(
        title: _namecontroller.text,
        director: _directorcontroller.text,
        summary: _summarycontroller.text,
        genres: genreSelected));

    moviesBloc.changeMoviesList(moviesBloc.moviesListValue ?? []);
    GoRouter.of(context).pop();
  }

  void updateMovie() {
    if (_namecontroller.text.trim().isEmpty) {
      context.showMessageDialog(nameCannotBeEmpty);
      return;
    }

    if (_directorcontroller.text.isEmpty) {
      context.showMessageDialog(directorCannotBeEmpty);
      return;
    }

    if (_summarycontroller.text.isEmpty) {
      context.showMessageDialog(summaryCannotBeEmpty);
      return;
    }

    if (genreSelected.isEmpty) {
      context.showMessageDialog(genreCannotBeEmpty);
      return;
    }
    var movieList = moviesBloc.moviesListValue;

    var toRemove = [];
    movieList?.forEach((e) {
      if (e.title == widget.name &&
          e.director == widget.director &&
          e.summary == widget.summary) {
        toRemove.add(e);
      }
    });
    movieList?.removeWhere((e) => toRemove.contains(e));

    movieList?.add(Movies(
        title: _namecontroller.text,
        director: _directorcontroller.text,
        summary: _summarycontroller.text,
        genres: genreSelected));

    moviesBloc.changeMoviesList(movieList ?? []);
    GoRouter.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            GoRouter.of(context).pop();
          },
          icon: const Icon(Icons.arrow_back),
        ),
        actions: [
          widget.name.trim().isEmpty
              ? TextButton(
                  onPressed: addMovie,
                  child: const Text(
                    save,
                    style: TextStyle(color: Colors.black),
                  ),
                )
              : TextButton(
                  onPressed: updateMovie,
                  child: const Text(
                    update,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextField(
              controller: _namecontroller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: name,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: TextField(
                controller: _directorcontroller,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: director,
                ),
              ),
            ),
            TextField(
              maxLines: 5,
              controller: _summarycontroller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: summary,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Flex(
                direction: Axis.horizontal,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (genreSelected.contains(drama)) {
                          genreSelected.remove(drama);
                        } else {
                          genreSelected.add(drama);
                        }
                      });
                    },
                    child: Chip(
                      backgroundColor: genreSelected.contains(drama)
                          ? Colors.red
                          : Colors.amber,
                      label: const Text(drama),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          if (genreSelected.contains(action)) {
                            genreSelected.remove(action);
                          } else {
                            genreSelected.add(action);
                          }
                        });
                      },
                      child: Chip(
                        backgroundColor: genreSelected.contains(action)
                            ? Colors.red
                            : Colors.amber,
                        label: const Text(action),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          if (genreSelected.contains(scifi)) {
                            genreSelected.remove(scifi);
                          } else {
                            genreSelected.add(scifi);
                          }
                        });
                      },
                      child: Chip(
                        backgroundColor: genreSelected.contains(scifi)
                            ? Colors.red
                            : Colors.amber,
                        label: const Text(scifi),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (genreSelected.contains(animation)) {
                          genreSelected.remove(animation);
                        } else {
                          genreSelected.add(animation);
                        }
                      });
                    },
                    child: Chip(
                      backgroundColor: genreSelected.contains(animation)
                          ? Colors.red
                          : Colors.amber,
                      label: const Text(animation),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
