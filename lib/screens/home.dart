import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:movies/bloc/movies_bloc.dart';
import 'package:movies/model/movies.dart';
import 'package:movies/utils/constants.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();

    moviesBloc.moviesListValue?.add(Movies(
        title: movieName,
        director: movieDirector,
        summary: movieSummary,
        genres: [drama, action]));
  }

  @override
  void dispose() {
    moviesBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: StreamBuilder<String>(
              stream: moviesBloc.movieTitle,
              builder: (context, snapshot) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        onChanged: (value) {
                          moviesBloc.changeMoviesTitle(value);
                          List<Movies> list = [];
                          if (moviesBloc.moviesListValue?.isNotEmpty == true) {
                            // for (final data in !) {}
                            moviesBloc.moviesListValue?.forEach((e) {
                              if (e.title?.toLowerCase().contains(value) ==
                                  true) {
                                list.add(e);
                              }
                            });
                          }

                          moviesBloc.changeSearchedMoviesList(list);
                        },
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: searchMovieByTitle,
                        ),
                      ),
                    ),
                    moviesBloc.movieTitleValue?.isEmpty == true ||
                            moviesBloc.movieTitleValue == ""
                        ? StreamBuilder<List<Movies>>(
                            stream: moviesBloc.moviesList,
                            builder: (context, snapshot) {
                              return Expanded(
                                child: ListView.builder(
                                  itemCount:
                                      moviesBloc.moviesListValue?.length ?? 0,
                                  itemBuilder: (context, index) {
                                    final movie =
                                        moviesBloc.moviesListValue?[index];

                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: InkWell(
                                        onTap: () {
                                          GoRouter.of(context).pushNamed(
                                              homeDetailRoute,
                                              params: {
                                                "name": movie?.title ?? "",
                                                "director":
                                                    movie?.director ?? "",
                                                "summary": movie?.summary ?? "",
                                                "genres":
                                                    movie?.genres?.join(", ") ??
                                                        ""
                                              });
                                        },
                                        child: Card(
                                          child: ListTile(
                                            title: Text(movie?.title ?? ""),
                                            subtitle:
                                                Text(movie?.summary ?? ""),
                                            leading:
                                                Text(movie?.director ?? ""),
                                            trailing:
                                                movie?.genres?.isNotEmpty ??
                                                        false
                                                    ? Column(
                                                        children: movie!.genres!
                                                            .map((item) => Text(
                                                                  item,
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          8),
                                                                ))
                                                            .toList())
                                                    : Container(),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              );
                            })
                        : StreamBuilder<List<Movies>>(
                            stream: moviesBloc.searchedMovieList,
                            builder: (context, snapshot) {
                              return Expanded(
                                child: ListView.builder(
                                  itemCount: moviesBloc
                                          .searchedMovieListValue?.length ??
                                      0,
                                  itemBuilder: (context, index) {
                                    final movie = moviesBloc
                                        .searchedMovieListValue?[index];

                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: InkWell(
                                        onTap: () {
                                          GoRouter.of(context).pushNamed(
                                              homeDetailRoute,
                                              params: {
                                                "name": movie?.title ?? "",
                                                "director":
                                                    movie?.director ?? "",
                                                "summary": movie?.summary ?? "",
                                                "genres":
                                                    movie?.genres?.join(", ") ??
                                                        ""
                                              });
                                        },
                                        child: Card(
                                          child: ListTile(
                                            title: Text(movie?.title ?? ""),
                                            subtitle:
                                                Text(movie?.summary ?? ""),
                                            leading:
                                                Text(movie?.director ?? ""),
                                            trailing:
                                                movie?.genres?.isNotEmpty ??
                                                        false
                                                    ? Column(
                                                        children: movie!.genres!
                                                            .map((item) => Text(
                                                                  item,
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          8),
                                                                ))
                                                            .toList())
                                                    : Container(),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              );
                            }),
                  ],
                );
              }),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            GoRouter.of(context).pushNamed(homeDetailRoute, params: {
              "name": " ",
              "director": " ",
              "summary": " ",
              "genres": " "
            });
          },
          child: const Icon(Icons.add),
        ));
  }
}
