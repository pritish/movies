import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movies/utils/constants.dart';

extension DialogExtensions on BuildContext {
  void showMessageDialog(String msg) {
    showDialog(
        context: this,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(
              msg,
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text(ok),
              )
            ],
          );
        });
  }
}
