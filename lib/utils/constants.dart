//Constants
const appName = 'Movies';
const name = 'Name';
const director = 'Director';
const summary = 'Summary';
const movieName = 'Laal Singh Chadda';
const movieDirector = 'Aamir khan';
const movieSummary =
    'Nice movie to watch to get inspired from and be humble and simple in what you do';
const drama = 'Drama';
const action = 'Action';
const scifi = 'SciFi';
const animation = 'Animation';
const save = "Save";
const update = "Update";
const ok = 'Ok';
const nameCannotBeEmpty = "Name cannot be empty";
const directorCannotBeEmpty = "Director cannot be empty";
const summaryCannotBeEmpty = "Summary cannot be empty";
const genreCannotBeEmpty = "Genre cannot be empty";
const searchMovieByTitle = "Search movie by title";

//Routes
const homeDetailRoute = '/home-detail';
