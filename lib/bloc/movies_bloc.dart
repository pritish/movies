import 'package:movies/model/movies.dart';
import 'package:rxdart/rxdart.dart';

class MoviesBloc {
  final _moviesList = BehaviorSubject<List<Movies>>.seeded([]);
  final _searchedMovieList = BehaviorSubject<List<Movies>>.seeded([]);
  final _movieTitle = BehaviorSubject<String>.seeded("");

  Stream<List<Movies>> get moviesList => _moviesList.stream;
  Stream<List<Movies>> get searchedMovieList => _searchedMovieList.stream;
  Stream<String> get movieTitle => _movieTitle.stream;

  List<Movies>? get moviesListValue => _moviesList.stream.valueOrNull;
  List<Movies>? get searchedMovieListValue =>
      _searchedMovieList.stream.valueOrNull;
  String? get movieTitleValue => _movieTitle.stream.valueOrNull;

  Function(List<Movies>) get changeMoviesList => _moviesList.sink.add;
  Function(List<Movies>) get changeSearchedMoviesList =>
      _searchedMovieList.sink.add;
  Function(String) get changeMoviesTitle => _movieTitle.sink.add;

  void dispose() {
    _moviesList.close();
    _searchedMovieList.close();
    _movieTitle.close();
  }
}

final moviesBloc = MoviesBloc();
