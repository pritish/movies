import 'package:flutter/material.dart';

class Movies {
  Movies(
      {@required this.title,
      @required this.director,
      @required this.summary,
      @required this.genres});

  final String? title;
  final String? director;
  final String? summary;
  final List<String>? genres;
}
