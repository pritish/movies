import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:movies/screens/home-detail.dart';
import 'package:movies/screens/home.dart';
import 'package:movies/utils/constants.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationProvider: _router.routeInformationProvider,
      title: appName,
      debugShowCheckedModeBanner: false,
      routeInformationParser: _router.routeInformationParser,
      routerDelegate: _router.routerDelegate,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}

final _router = GoRouter(routes: [
  GoRoute(
    path: '/',
    builder: (context, state) => const Home(),
  ),
  GoRoute(
    name: homeDetailRoute,
    path: '$homeDetailRoute/:name/:director/:summary/:genres',
    builder: (context, state) {
      final name = state.params['name']!;
      final director = state.params['director']!;
      final summary = state.params['summary']!;
      final genres = state.params['genres']!;
      return HomeDetail(
        name: name,
        summary: summary,
        director: director,
        genres: genres,
      );
    },
  ),
  // GoRoute(
  //   name: homeDetailRoute,
  //   path: homeDetailRoute,
  //   builder: (context, state) => HomeDetail(
  //     name: "",
  //     summary: "",
  //     director: "",
  //     genres: "",
  //   ),
  // ),
]);
